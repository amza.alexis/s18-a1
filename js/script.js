console.log("Hello World!");

function trainer(name, age, lvl){
	this.name = name;
	this.age = age;
	this.lvl = lvl;
};

let player = new trainer("Amzel", 23, "Level 50");
console.log(player);

function Pokemon(name, health){
	this.name = name;
	this.health = health;
	this.attack = function(target, move){
		console.log(`${this.name} used ${move} to ${target.name}. Boom`);
		target.health -= 100;
		console.log(`${target.name}'s health is now ${target.health}`)
	}
};

let Mewtwo = new Pokemon("Mewtwo", 1000);
let Arceus = new Pokemon("Arceus", 750);
let Mew = new Pokemon("Mew", 840);

Mewtwo.attack(Mew, "Psystrike");
Mew.attack(Mewtwo, "Charge Beam");
Arceus.attack(Mew, "Shadow Claw");
